package course.design_patterns.adapter;

import com.sparkpost.Client;
import com.sparkpost.exception.SparkPostException;

/**
 * Adaptador (adapter).
 *
 * Esta es la clase concreta que debe hacer la conversión entre lo que entiende nuestro sistema al adaptee (en este
 * caso, la librería de SparkPost).
 *
 * Debe implementar la interfaz que representa el objetivo. Para nuestro ejemplo, EmailSender.
 *
 * @author Manuel Zapata
 * */
class SparkPostAdapter implements EmailSender {

    @Override
    public void send(String from, String to, String subject, String body) {

        final String API_KEY = "SPARKPOST_API_KEY";

        //En este caso, el adaptee es la clase Client en la libreria de SparkPost.
        Client client = new Client(API_KEY);

        try {
            //La adaptación ocurre al saber qué y cómo tenemos que utilizar la librería de SparkPost.
            client.sendMessage(from, to, subject, body, null);
            System.out.println("Mensaje enviado con el adaptador de SparkPost");
        } catch (SparkPostException e) {
            e.printStackTrace();
            System.out.println("No se pudo enviar mensaje con el adaptador de SparkPost");
        }

    }
}
