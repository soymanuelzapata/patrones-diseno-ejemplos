package course.design_patterns.command;

import java.util.Random;

/**
 * Invocador (invoker).
 *
 * Es quien solicita la ejecución del comando, para que en el algún momento el receptor reciba y complete la solicitud.
 *
 * @author Manuel Zapata
 */
class Form {

    private Command command;

    /**
     * Pasamos como parámetro el comando que tiene que ejecutar este invocador.
     * */
    public void setCommand(Command command){
        this.command = command;
    }

    void save() {
        System.out.println("Desde el método save del formulario");
        //Obtener datos para crear el objeto Producto.
        Product product = getProduct();

        /*
         * El invocador sabe en qué momento debe llamar el comando. Por ejemplo, cuando opriman un botón en la
         * interfaz gráfica, o cuando hagan una solicitud al servidor.
         */
        command.execute(product);

    }

    private Product getProduct() {
        int id = new Random().nextInt(1000);
        Product product = new Product(id, "Product " + id);
        return product;
    }

}
