package course.design_patterns.command;

/**
 * Este objeto no hace parte como tal de la estructura del patrón.
 *
 * Es solo un objeto complementario para facilitar el ejemplo.
 *
 * @author Manuel Zapata
 */
class Product {

    private int id;
    private String name;

    public Product(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
