package course.design_patterns.template_method;

/**
 * Clase concreta (concrete class).
 *
 * Representa una implementación de la clase abstracta con el método plantilla. La idea es que la clase defina los
 * métodos abstractos o pasos que no pueden ser definidos en la clase padre.
 *
 * En este ejemplo, la clase representa una implementación donde la cuenta debe ser creada en una plataforma en la nube.
 *
 * @author Manuel Zapata
 * */
class CloudAccountCreator extends AccountCreator {
    @Override
    protected void savePicture(byte[] picture) {
        System.out.println("Guardando imagen en el servidor de storage de la plataforma");
    }

    @Override
    protected void sendVerificationEmail(String email) {
        System.out.println("Enviando correo a través de servicio masivo de emails");
    }

    /**
     * Al ser un hook, no es necesario definir el método, a menos que se necesite.
     * */
    @Override
    protected void log(Account account) {
        System.out.println("Guardando log de la creación de la cuenta.");
    }
}
