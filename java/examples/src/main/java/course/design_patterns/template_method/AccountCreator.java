package course.design_patterns.template_method;

/**
 * Clase abstracta (abstract class).
 *
 * Esta clase define los métodos (pasos del algoritmo) que deben ser implementados en las clases hijas.
 *
 * También implementa el método plantilla (template method), el cual define el orden en que se deben ejecutar los pasos.
 *
 * @author Manuel Zapata
 * */
abstract class AccountCreator {

    /**
     * Este es el método plantilla.
     *
     * Es "final" para que no pueda ser sobreescrito o modificado en las clases hijas.
     *
     * Los pasos del algoritmo están representados por llamados a otros métodos.
     *
     * */
    final void execute(Account account) {

        boolean accountSaved = saveAccountData(account);
        if(accountSaved) {
            sendVerificationEmail(account.getEmail());
            savePicture(account.getPicture());
            log(account);
        }

    }

    /**
     * Uno de los pasos que debe ser implementado en las clases hijas.
     * */
    protected abstract void savePicture(byte[] picture);

    /**
     * Uno de los pasos que debe ser implementado en las clases hijas.
     * */
    protected abstract void sendVerificationEmail(String email);

    /**
     * Este es un hook (gancho).
     *
     * Es un método que podría ser sobreescrito de manera opcional en las clases hijas.
     * */
    protected void log(Account account) {

    }

    /**
     * Este no es el método plantilla, pero aún así lo hemos declarado "final" (es decir, no puede ser sobreescrito).
     *
     * La razón es que contiene una lógica que no queremos que sea alterada por clases hijas.
     *
     * */
    final boolean saveAccountData(Account account) {

        //Guardar la cuenta en algún lado.
        System.out.println("Guardar la cuenta en algún lado...");

        return true;

    }


}
