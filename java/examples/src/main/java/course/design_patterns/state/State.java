package course.design_patterns.state;

import java.time.Duration;

/**
 * Estado (state).
 *
 * Esta interfaz debe ser implementada por todos los posibles estados del contexto (en este caso, MicrowaveOven).
 *
 * @author Manuel Zapata
 * */
public interface State {

    /**
     * Este método y los siguientes son específicos al problema. La idea es que sean definidos a partir del
     * comportamiento que puede variar dependiendo del estado del contexto.
     * */
    boolean start(Duration duration);

    boolean stop();

    boolean openDoor();

}
