package course.design_patterns.strategy;

import javax.crypto.*;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * Estrategia concreta (concrete strategy).
 *
 * Representa una de los algoritmos soportados por el sistema. Implementa la interfaz Strategy (EncryptionStrategy en
 * este caso).
 *
 * En este ejemplo en particular, la clase representa el algoritmo de encripción simétrica AES.
 *
 * @author Manuel Zapata
 * */
class AESStrategy implements EncryptionStrategy {

    private KeyGenerator generator;
    private Key key;
    private Cipher cipher;

    @Override
    public byte[] encrypt(String text) {

        byte[] result = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            result = cipher.doFinal(text.getBytes());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    @Override
    public String decrypt(byte[] textToDecrypt) {

        String result = null;
        try {
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decrypted = cipher.doFinal(textToDecrypt);
            result = new String(decrypted);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean init() {
        boolean result = false;
        try {
            generator = KeyGenerator.getInstance("AES");
            generator.init(128);
            key = generator.generateKey();
            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            result = true;
        } catch (NoSuchAlgorithmException exception) {
            exception.printStackTrace();
        } catch (NoSuchPaddingException exception) {
            exception.printStackTrace();
        }
        return result;
    }


}
