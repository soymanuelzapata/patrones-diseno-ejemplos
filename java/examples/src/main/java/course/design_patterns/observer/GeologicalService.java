package course.design_patterns.observer;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Sujeto concreto (concrete subject).
 *
 * Los objetos de esta clase son capaces de generar las notificaciones que escucharán los observadores.
 *
 * Debe implementar la interfaz Subject.
 *
 * Para este ejemplo, la clase representa un servicio geológico que es capaz de enterarse e informar cuando un
 * terremoto ocurre.
 *
 * @author Manuel Zapata
 * */
class GeologicalService implements Subject {

    //En esta lista se guardarán los observadores.
    private List<Observer> observers;

    public GeologicalService() {
        observers = new ArrayList<>();
    }

    /**
     * En la interfaz Subject está la descripción de este y los siguientes métodos.
     * */
    @Override
    public void subscribe(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void unsubscribe(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(Earthquake earthquake) {

        System.out.println("Antes de notificar a los observadores");
        //Informamos a cada uno de los observadores que el evento ocurrió.
        for(Observer observer: observers) {
            observer.update(earthquake);
        }
        System.out.println("Los observadores han sido notificados");

    }

    /**
     * Con este método vamos a simular que el terremoto ocurrió.
     *
     * Dependiendo de la implementación, un tercero le podría decir al sujeto que notifique,
     * pero el sujeto también podría detectar con su lógica interna que el evento ocurrió.
     * */
    public void tremble() {

        //Objeto con datos de prueba que representa el evento.
        Earthquake earthquake = new Earthquake(30.3207200, 35.4839200, 6,
                LocalDateTime.now());
        //Como sabemos que el evento "ocurrió", debemos notificar a los observadores.
        notifyObservers(earthquake);

    }
}
