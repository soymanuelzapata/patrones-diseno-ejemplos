package course.design_patterns.composite;

/**
 * Hoja (leaf).
 *
 * Es una clase hija del componente, la cual no tiene relación de composición con otros componentes. Es decir, no
 * tiene hijos.
 *
 * Estas son las clases que hacen el trabajo, y no lo delegan, como sí lo hacen los objetos compuestos.
 *
 * @author Manuel Zapata
 */
public class Engineer extends Employee {

    public Engineer(String name, String positionName) {
        super(name, positionName);
    }

    /**
     * La hoja implementa el comportamiento que el componente padre espera.
     * */
    @Override
    void assignProject(String projectName) {
        this.setCurrentProject(projectName);
    }

    /**
     * La hoja implementa el comportamiento que el componente padre espera.
     * */
    @Override
    void print() {
        String message = getName() + " - " + getPositionName() + " - " + getCurrentProject();
        System.out.println(message);
    }

    /**
     * En este método lanzamos una excepción, porque las hojas no tienen relación con otros componentes.
     * */
    @Override
    void add(Employee employee) {
        throw new UnsupportedOperationException();
    }

    /**
     * En este método lanzamos una excepción, porque las hojas no tienen relación con otros componentes.
     * */
    @Override
    void remove(Employee employee) {
        throw new UnsupportedOperationException();
    }

    /**
     * En este método lanzamos una excepción, porque las hojas no tienen relación con otros componentes.
     * */
    @Override
    Employee getEmployee(int index) {
        throw new UnsupportedOperationException();
    }
}
