package course.design_patterns.factory_method.simple_factory;

/**
 * Producto (product).
 *
 * Interfaz que deben implementar los objetos que serán creados por la fábrica sencilla.
 *
 * @author Manuel Zapata
 */
interface Game {
}
