package course.design_patterns.builder;

import course.design_patterns.builder.entities.Accommodation;
import course.design_patterns.builder.entities.AccommodationType;
import course.design_patterns.builder.entities.StudyProgram;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Constructor concreto (concrete builder).
 *
 * Representa un constructor especializado en la creación de cierto producto. Implementa la interfaz
 * Builder (PlanBuilder en este caso).
 *
 * En este ejemplo en particular, la clase se especializa en construir objetos de tipo InternationalPlan.
 *
 * La lógica en cada método (paso de construcción) depende específicamente del producto.
 *
 * @author Manuel Zapata
 * */
class InternationalPlanBuilder implements PlanBuilder {

    //Este es el producto que este builder va a construir
    private InternationalPlan plan;

    public InternationalPlanBuilder() {
        reset();
    }

    @Override
    public void definePeriod(LocalDate startDate, LocalDate endDate) {
        plan.setStartDate(startDate);
        plan.setEndDate(endDate);
    }

    @Override
    public void defineStudyProgram(int programId, int universityId) {
        StudyProgram program = new StudyProgram(programId, universityId);
        plan.setStudyProgram(program);
    }


    @Override
    public boolean defineAccommodation(AccommodationType type, LocalDate startDate, LocalDate endDate) {

        boolean result = false;

        if(type == AccommodationType.HOTEL) {

            long days = ChronoUnit.DAYS.between(startDate, endDate);
            if(days < 15) {
                result = true;
                Accommodation accommodation = new Accommodation(type, startDate, endDate);
                plan.addAccommodationItem(accommodation);
            }
        } else {
            Accommodation accommodation = new Accommodation(type, startDate, endDate);
            plan.addAccommodationItem(accommodation);
            result = true;
        }

        return result;
    }

    /**
     * En este caso, para hacerle reset al constructor, basta con crear un nuevo producto.
     * */
    @Override
    public void reset() {
        plan = new InternationalPlan();
    }


    /**
     * Este método nos debe devolver el producto que esta clase ha estado construyendo.
     *
     * No está definido en la interfaz Builder (PlanBuilder en este caso) porque los productos no necesariamente
     * pertenecen a la misma jerarquía.
     * */
    public InternationalPlan getPlan() {
        InternationalPlan builtPlan = plan;
        reset();
        return builtPlan;
    }
}
