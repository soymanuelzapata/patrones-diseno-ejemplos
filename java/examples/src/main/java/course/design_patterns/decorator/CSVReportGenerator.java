package course.design_patterns.decorator;

import java.io.*;
import java.util.List;
import java.util.UUID;

/**
 * Componente concreto (concrete component).
 *
 * Esta es la clase concreta cuya funcionalidad será extendida a través de composición y decoradores.
 *
 * En nuestro caso, es una clase que genera un archivo CSV con unos datos que le llegan como parámetro.
 *
 * Debe implementar la interfaz que representa el componente. Para nuestro ejemplo, SalesReportGenerator.
 *
 * @author Manuel Zapata
 * */
public class CSVReportGenerator implements SalesReportGenerator {

    /**
     * En la implementación del método, definimos la funcionalidad básica que luego podrá ser ampliada.
     * */
    @Override
    public String generate(List<ReportItem> items) {

        File file = new File("reporte_" + UUID.randomUUID() +".csv");

        try(PrintWriter writer = new PrintWriter(new FileWriter(file))) {

            writer.println("ID,Nombre cliente,Fecha,Valor");
            for(ReportItem item: items) {
                writer.printf("%s,%s,%s,%s",
                        item.getSaleId(),
                        item.getCustomerName(),
                        item.getDate(),
                        item.getAmount());
                writer.println();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Generación de archivo CSV completada.");
        return file.getAbsolutePath();
    }

}
