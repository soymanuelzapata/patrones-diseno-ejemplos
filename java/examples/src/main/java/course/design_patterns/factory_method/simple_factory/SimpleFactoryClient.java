package course.design_patterns.factory_method.simple_factory;


/**
 * Clase ejecutable para observar cómo se utiliza la fábrica sencilla
 *
 * @author Manuel Zapata
 * */
public class SimpleFactoryClient {

    public static void main(String[] args) {

        Game one = SimpleFactory.createGame("chess");
        System.out.println(one.getClass().getSimpleName());

        Game two = SimpleFactory.createGame("solitaire");
        System.out.println(two.getClass().getSimpleName());

    }

}
