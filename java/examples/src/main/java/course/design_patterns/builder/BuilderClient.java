package course.design_patterns.builder;

/**
 * Clase ejecutable que nos ayuda a ver cómo se utilizan el director y los constructores.
 *
 * @author Manuel Zapata
 */
class BuilderClient {

    public static void main(String[] args) {

        LocalPlanBuilder localBuilder = new LocalPlanBuilder();

        Director director = new Director(localBuilder);

        //Le pedimos al director que construya el producto a partir de algoritmo que tiene definido.
        director.constructMBAPlan();

        //Una vez construido el producto, pedimos al constructor que nos lo dé.
        LocalPlan localPlan = localBuilder.getPlan();

        InternationalPlanBuilder internationalBuilder = new InternationalPlanBuilder();
        //Asignamos un nuevo constructor al director.
        director.setBuilder(internationalBuilder);
        //Con un nuevo builder asignado, pedimos al director que construya un nuevo plan.
        director.constructMBAPlan();

        InternationalPlan internationalPlan = internationalBuilder.getPlan();

        System.out.println(localPlan);
        System.out.println(internationalPlan);

    }

}
