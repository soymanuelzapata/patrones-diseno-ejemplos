package course.design_patterns.builder;

import course.design_patterns.builder.entities.Accommodation;
import course.design_patterns.builder.entities.StudyProgram;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Producto.
 *
 * Representa uno de los productos que puede crear un constructor (builder).
 *
 * A diferencia de otros patrones, este producto no tiene que pertenecer a ninguna jerarquía de clases.
 *
 * La estructura del producto depende totalmente del sistema donde se esté usando el patrón.
 *
 * @author Manuel Zapata
 * */
class InternationalPlan {

    private LocalDate startDate;
    private LocalDate endDate;
    private StudyProgram studyProgram;

    private List<Accommodation> accommodationList;

    public InternationalPlan() {
        accommodationList = new ArrayList<>();
    }

    public void addAccommodationItem(Accommodation accommodation) {
        accommodationList.add(accommodation);
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public StudyProgram getStudyProgram() {
        return studyProgram;
    }

    public void setStudyProgram(StudyProgram studyProgram) {
        this.studyProgram = studyProgram;
    }

    public String toString() {
        return "Plan internacional desde " + startDate + " hasta " + endDate;
    }

}
