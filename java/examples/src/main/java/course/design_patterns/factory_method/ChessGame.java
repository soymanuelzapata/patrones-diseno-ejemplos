package course.design_patterns.factory_method;

/**
 * Producto concreto (concrete product).
 *
 * Representa uno de los productos que será creado por el método fábrica.
 *
 * El patrón no define qué atributos o métodos deben tener los productos concretos. Eso queda a consideración del
 * diseñador del sistema.
 *
 * @author Manuel Zapata
 */
class ChessGame implements Game {

    private String playerOne;
    private String playerTwo;

    public ChessGame(String playerOne, String playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    @Override
    public void start() {
        System.out.println("Inicia juego de ajedrez");
    }
}
