package course.design_patterns.factory_method;

/**
 * Esta clase no hace parte de la estructura del patrón, pero es importante porque nos permite ver como se configuran
 * los objetos.
 *
 * @author Manuel Zapata
 */
public class FactoryMethodClient {

    public static void main(String[] args) {

        //Crear tablero de ajedrez.

        String[] chessPlayers = new String[] { "Peter Parker", "Clark Kent" };

        Board chessBoard = new ChessBoard(chessPlayers);
        chessBoard.initialize();

        //Crear tablero de solitario
        String[] solitairePlayer = new String[] { "Jack Sparrow" };

        Board solitaireBoard = new SolitaireBoard(solitairePlayer);
        solitaireBoard.initialize();

    }

}
