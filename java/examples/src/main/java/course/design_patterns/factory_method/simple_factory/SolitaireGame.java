package course.design_patterns.factory_method.simple_factory;

/**
 * Producto concreto (concrete product).
 *
 * Representa uno de los productos que será creado por la fábrica sencilla.
 *
 * @author Manuel Zapata
 */
class SolitaireGame implements Game {
}
