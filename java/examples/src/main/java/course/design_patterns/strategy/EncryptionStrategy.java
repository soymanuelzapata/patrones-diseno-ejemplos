package course.design_patterns.strategy;

/**
 * Estrategia (strategy).
 *
 * Esta interfaz debe ser implementada por cada una de las clases que tendrán las estrategias concretas (algoritmos).
 *
 * En este ejemplo en particular, tenemos distintas estrategias de algoritmos para encriptar datos de forma simétrica.
 *
 * @author Manuel Zapata
 * */
interface EncryptionStrategy {

    byte[] encrypt(String text);

    String decrypt(byte[] textToDecrypt);

    boolean init();

}
