package course.design_patterns.observer;

/**
 * Observador (observer).
 *
 * Esta interfaz debe ser implementada por todos los observadores concretos, los cuales serán notificados cuando un
 * evento ocurra.
 *
 * @author Manuel Zapata
 * */
interface Observer {

    /**
     * Este método será invocado cuando el evento o notificación pase.
     *
     * El parámetro, en este caso, es un objeto específico del ejemplo, pero también podría ser un objeto más genérico,
     * que represente un evento general del sistema.
     *
     * */
    void update(Earthquake earthquake);

}
