package course.design_patterns.composite;

/**
 * Cliente (client).
 *
 * Clase que utilizará la jerarquía, a partir del elemento raíz.
 *
 * @author Manuel Zapata
 */
class Department {

    //Componente raíz de la jerarquía. Podría ser tanto una hoja como un objeto complejo.
    private Employee leader;

    public Department(Employee leader) {
        this.leader = leader;
    }

    /**
     * Para implementar el método, se le cede el control al componente.
     * */
    public void printMembers() {
        leader.print();
    }
}
