package course.design_patterns.proxy;

public class ProxyApp {

    public static void main(String[] args) {

        //El objeto que tendrá la operación que queremos ejecutar.
        UserService userService = new BaseUserService();
        //El objeto proxy.
        UserService userProxy = new UserServiceLogger(userService);

        //Ahora ejecutamos el cliente, y le pasamos el sujeto que debe utilizar.
        ProxyClient client = new ProxyClient(userProxy);
        client.saveUser("hola@manuelzapata.co");
        client.saveUser("hola@manuelzapata.co");

    }

}
