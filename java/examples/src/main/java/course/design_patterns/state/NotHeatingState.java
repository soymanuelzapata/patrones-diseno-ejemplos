package course.design_patterns.state;

import java.time.Duration;

/**
 * Estado concreto (concrete state).
 *
 * Representa uno de los estados concretos del contexto (clase MicrowaveOven en este caso). Implementa la interfaz
 * State.
 *
 * En este ejemplo en particular, la clase representa el estado en el cual un microonandas NO se encuentra calentando.
 *
 * Ver la clase HeatingState para una explicación detallada de las partes de un estado concreto.
 *
 * @author Manuel Zapata
 * */
class NotHeatingState implements State {

    private MicrowaveOven context;

    public NotHeatingState(MicrowaveOven context) {
        this.context = context;
    }

    @Override
    public boolean start(Duration duration) {
        //Lógica de negocio
        context.changeState(new HeatingState(context));
        return true;
    }

    @Override
    public boolean stop() {
        return false;
    }

    @Override
    public boolean openDoor() {
        return true;
    }

}
