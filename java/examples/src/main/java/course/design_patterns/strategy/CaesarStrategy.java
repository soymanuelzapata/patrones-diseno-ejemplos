package course.design_patterns.strategy;

import java.util.Random;

/**
 * Estrategia concreta (concrete strategy).
 *
 * Representa una de los algoritmos soportados por el sistema. Implementa la interfaz Strategy (EncryptionStrategy en
 * este caso).
 *
 * En este ejemplo en particular, la clase representa un algoritmo muy básico de encripción simétrica llamado César.
 *
 * @author Manuel Zapata
 * */
class CaesarStrategy implements EncryptionStrategy {

    private static String ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ,.-";
    private int shift;

    @Override
    public byte[] encrypt(String text) {

        StringBuilder encryptedText = new StringBuilder();
        for (int index = 0; index < text.length(); index++)
        {
            int charPosition = ALPHABET.indexOf(text.charAt(index));
            int newCharacterPosition = (shift + charPosition) % ALPHABET.length();
            char newCharacter = ALPHABET.charAt(newCharacterPosition);
            encryptedText.append(newCharacter);
        }

        byte[] result = encryptedText.toString().getBytes();
        return result;
    }

    @Override
    public String decrypt(byte[] textToDecrypt) {

        String encryptedText = new String(textToDecrypt);
        StringBuilder result = new StringBuilder();
        for (int index = 0; index < encryptedText.length(); index++)
        {
            int alphabetPosition = ALPHABET.indexOf(encryptedText.charAt(index));
            int newCharacterPosition = (alphabetPosition - shift) % ALPHABET.length();
            if (newCharacterPosition < 0)
            {
                newCharacterPosition = ALPHABET.length() + newCharacterPosition;
            }
            char newCharacter = ALPHABET.charAt(newCharacterPosition);
            result.append(newCharacter);
        }
        return result.toString();
    }

    @Override
    public boolean init() {
        Random random = new Random();
        shift = random.nextInt(26);
        return true;
    }
}
