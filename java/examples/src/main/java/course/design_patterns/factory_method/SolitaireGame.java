package course.design_patterns.factory_method;

/**
 * Producto concreto (concrete product).
 *
 * Ver la documentación de la clase ChessGame para más detalles.
 *
 * @author Manuel Zapata
 */
class SolitaireGame implements Game {

    private String player;

    public SolitaireGame(String player) {
        this.player = player;
    }

    @Override
    public void start() {
        System.out.println("Inicia juego de solitario");
    }
}
