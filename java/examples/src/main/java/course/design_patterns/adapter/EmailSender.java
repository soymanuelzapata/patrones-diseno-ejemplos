package course.design_patterns.adapter;

/**
 * Objetivo (target).
 *
 * Esta interfaz debe ser implementada por cada uno de los adaptadores, y debe ser utilizada por el cliente.
 *
 * @author Manuel Zapata
 * */
interface EmailSender {

    /**
     * Este es el método donde se debe adaptar lo que "entiende" nuestro sistema a lo que entiende el adaptee
     * (librería, framework o subsistema que estamos incorporando).
     *
     * En este caso queremos ofrecer un método muy sencillo para enviar correos, que debe poder adaptarse a las
     * distintas plataformas y librerías de correo electrónico.
     *
     * */
    void send(String from, String to, String subject, String body);

}
