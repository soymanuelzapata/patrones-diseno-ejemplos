package course.design_patterns.builder.entities;

/**
 * Esta enumeración no hace parte de la estructura del patrón, pero nos ayuda a darle forma al producto que deben crear
 * los constructores.
 *
 * @author Manuel Zapata
 */
public enum AccommodationType {

    CAMPUS,
    FAMILY,
    HOTEL

}
