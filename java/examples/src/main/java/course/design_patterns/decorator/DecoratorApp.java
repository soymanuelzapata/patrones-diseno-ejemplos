package course.design_patterns.decorator;

class DecoratorApp {

    public static void main(String[] args) {
        //Creamos el componente concreto
        SalesReportGenerator csvReportGenerator = new CSVReportGenerator();
        //Creamos el decorador concreto, y le pasamos el componente que va a decorar.
        SalesReportGenerator obfuscatorGenerator = new ObfuscatorReportGenerator(csvReportGenerator);

        //Creamos el cliente y lo ejecutamos
        DecoratorClient client = new DecoratorClient();
        client.execute(csvReportGenerator);
        client.execute(obfuscatorGenerator);

    }

}
