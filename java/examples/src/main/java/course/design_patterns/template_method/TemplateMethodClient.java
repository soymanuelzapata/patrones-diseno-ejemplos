package course.design_patterns.template_method;

/**
 * Clase ejecutable que nos ayuda a ver cómo se utilizan las clases concretas.
 *
 * @author Manuel Zapata
 */
class TemplateMethodClient {

    public static void main(String[] args) {

        Account account = new Account("manuelzapata","password1", "hola@manuelzapata.co");
        account.setPicture(new byte[] {0, 1, 0});

        //Solo debemos crear el objeto de la clase concreta que necesitemos.
        AccountCreator onPremiseCreator = new OnPremiseAccountCreator();
        //Invocamos el método plantilla.
        onPremiseCreator.execute(account);

        System.out.println();

        AccountCreator cloudCreator = new CloudAccountCreator();
        cloudCreator.execute(account);

    }

}
