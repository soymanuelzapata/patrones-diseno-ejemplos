# Curso práctico de patrones de diseño

Repositorio oficial del Curso Práctico de Patrones de Diseño de Manuel Zapata. Aquí encontrarás ejemplos de implementación de patrones de diseño en distintos lenguajes de programación, talleres y proyecto completos donde se aplican los patrones.

## Patrones de diseño

1. Adapter (adaptador).
1. Builder (constructor).
1. Command (comando).
1. Composite (objeto compuesto).
1. Decorator (decorador).
1. Facade (fachada).
1. Factory Method (método fábrica).
1. Observer (observador).
1. Proxy.
1. Singleton.
1. State (estado).
1. Strategy (estrategia).
1. Template method (método plantilla).

## Sobre el Curso

Si deseas más información sobre curso, puedes ver más detalles [aquí](https://bit.ly/2JgTAgF).