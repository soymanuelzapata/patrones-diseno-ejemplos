package course.design_patterns.strategy;

/**
 * Clase que utiliza el contexto, y le indica qué estrategia utilizar.
 *
 * El cliente tener el conocimiento de las estrategias concretas y saber cuál necesita.
 *
 * @author Manuel Zapata
 */
class StrategyClient {

    public static void main(String[] args) {

        String originalText = "Los patrones son geniales";

        //Definimos la primera estrategia.
        EncryptionStrategy strategy = new CaesarStrategy();
        //Para crear el contexto se requiere una estrategia inicial.
        StrategyContext context = new StrategyContext(strategy);

        //Encriptamos y desencriptamos el texto a través del contexto, quien pasará la solicitud a la estrategia.
        byte[] encrypted = context.encryptText(originalText);
        String decrypted = context.decryptText(encrypted);

        System.out.println("Cesar: " + originalText);
        System.out.println("Cesar: " + decrypted);

        //Ahora probemos la segunda estrategia y pasemosla al contexto.
        strategy = new AESStrategy();
        context.setStrategy(strategy);

        encrypted = context.encryptText(originalText);
        decrypted = context.decryptText(encrypted);

        System.out.println("AES: " + originalText);
        System.out.println("AES: " + decrypted);

    }

}
