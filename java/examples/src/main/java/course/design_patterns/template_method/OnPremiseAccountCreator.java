package course.design_patterns.template_method;

/**
 * Clase concreta (concrete class).
 *
 * Representa una implementación de la clase abstracta con el método plantilla. La idea es que la clase defina los
 * métodos abstractos o pasos que no pueden ser definidos en la clase padre.
 *
 * En este ejemplo, la clase representa una implementación donde la cuenta debe ser creada en la infraestructura
 * propia de la empresa.
 *
 * @author Manuel Zapata
 * */
class OnPremiseAccountCreator extends AccountCreator {

    @Override
    protected void savePicture(byte[] picture) {
        System.out.println("Guardando imagen en el servidor");
    }

    @Override
    protected void sendVerificationEmail(String email) {
        System.out.println("Enviar correo usando servidor SMTP local");
    }

}
