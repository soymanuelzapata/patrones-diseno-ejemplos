package course.design_patterns.factory_method;

/**
 * Creador concreto (concrete creator).
 *
 * Es una clase hija del creador, la cual debe definir el método fábrica y retornar el tipo de producto que está
 * especializada en construir.
 *
 * El creador no solo tiene el método fábrica. También puede tener lógica específica a la responsabilidad de la clase.
 *
 * @author Manuel Zapata
 */
class ChessBoard extends Board {

    public ChessBoard(String[] players) {
        super(players);
    }

    /**
     * El método fábrica no se trata solo de crear un nuevo objeto y ya, sino que además puede tener lógica asociada
     * a la configuración del objeto.
     * */
    @Override
    Game createGame() {

        String[] players = getPlayers();

        String playerOne = "Blanco";
        String playerTwo = "Negro";

        if(players != null && players.length == 2) {
            playerOne = players[0];
            playerTwo = players[1];
        }

        ChessGame game = new ChessGame(playerOne, playerTwo);
        return game;
    }

}
