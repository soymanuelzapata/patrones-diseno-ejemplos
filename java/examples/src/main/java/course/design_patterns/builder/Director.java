package course.design_patterns.builder;

import course.design_patterns.builder.entities.AccommodationType;

import java.time.LocalDate;

/**
 * Director.
 *
 * Clase que hace uso de los constructores para ejecutar los pasos de construcción en cierto orden. Es útil en casos
 * donde existen ciertas configuraciones predefinidas para los productos.
 *
 * Esta clase es OPCIONAL, ya que el código cliente podría ejecutar directamente los pasos.
 *
 * */
class Director {

    //Referencia el constructor actual.
    private PlanBuilder builder;

    //Definimos un constructor (builder) inicial para el director.
    public Director(PlanBuilder builder) {
        this.builder = builder;
    }

    /**
     * Este método ofrece la construcción de un producto de acuerdo una secuencia definida.
     *
     * Podrían haber más métodos con otros pasos o secuencia para un producto.
     * */
    public void constructMBAPlan() {

        LocalDate now = LocalDate.now();

        //Asumimos que por defecto el plan dura 6 meses, y empieza 2 meses a partir de la fecha de hoy.
        LocalDate startDate = now.plusMonths(2);
        LocalDate endDate = now.plusMonths(8);

        builder.reset();
        builder.definePeriod(startDate, endDate);
        builder.defineAccommodation(AccommodationType.FAMILY, startDate, endDate);
        //Asumimos también que la universidad 10 y el programa 1 son los que se sugiere por defecto.
        builder.defineStudyProgram(1, 10);

    }

    /**
     * Abrimos la posibilidad a cambiar el builder.
     * */
    public void setBuilder(PlanBuilder newBuilder){
        builder = newBuilder;
        builder.reset();
    }

}
