package course.design_patterns.command;

import java.util.ArrayList;

/**
 * Receptor (receiver).
 *
 * Es quien en últimas ejecuta la acción solicitada por el invocador. El comando hace las veces de intermediario para
 * facilitar el reuso, y disminuir el acoplamiento entre invocador y receptor.
 *
 * El patrón no indica que este objeto implemente una interfaz en particular o tenga cierta estructura.
 *
 * @author Manuel Zapata
 */
class ProductService {

    private ArrayList<Product> products;

    public ProductService() {
        products = new ArrayList<>();
    }

    public void save(Product product) {
        System.out.println("Guardando producto " + product.getName());
        products.add(product);
    }

}
