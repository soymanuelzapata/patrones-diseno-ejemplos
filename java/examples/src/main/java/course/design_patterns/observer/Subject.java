package course.design_patterns.observer;

/**
 * Sujeto (subject).
 *
 * Esta interfaz debe ser implementada por todos los sujetos concretos; es decir, los objetos capaces de generar las
 * notificaciones que los observadores desean escuchar.
 *
 * En implementaciones sencillas, esta interfaz no es necesaria.
 *
 * @author Manuel Zapata
 * */
public interface Subject {

    /**
     * El observador se debe suscribir a través de este método, para que pueda ser notificado cuando el evento ocurra.
     * */
    void subscribe(Observer observer);

    /**
     * Cuando el observador no esté interesado en seguir escuchando eventos, se debe desuscribir usando esté método.
     * */
    void unsubscribe(Observer observer);

    /**
     * Como su nombre lo dice, este método permite notificar a los observadores registrados que el evento ocurrió.
     * */
    void notifyObservers(Earthquake earthquake);

}
