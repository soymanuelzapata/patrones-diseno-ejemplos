package course.design_patterns.factory_method;

/**
 * Producto (product).
 *
 * Interfaz que deben implementar los objetos que serán creados por los métodos fábrica. También podría ser una
 * clase abstracta.
 *
 * @author Manuel Zapata
 */
interface Game {

    /**
     * Para este ejemplo, definimos el método start() en esta interfaz.
     *
     * En tu implementación debes añadir los métodos que sean relevantes para tu sistema.
     * */
    void start();

}
