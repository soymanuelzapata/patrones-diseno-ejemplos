package course.design_patterns.proxy;

/**
 *
 * Sujeto (subject).
 *
 * Esta interfaz debe ser implementada tanto por los proxies como por los sujetos que realizan la operación.
 *
 * @author Manuel Zapata
 * */
interface UserService {

    /**
     * Este método representa la operación que el proxy quiere controlar.
     * */
    boolean save(String username);

}
