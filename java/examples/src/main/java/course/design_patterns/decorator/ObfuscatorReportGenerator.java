package course.design_patterns.decorator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Decorador concreto(concrete decorator).
 *
 * Esta es la clase que tiene la lógica necesaria para hacer la decoración
 *
 * Debe heredar del decorador. Para nuestro ejemplo, ReportDecorator.
 *
 * @author Manuel Zapata
 * */
class ObfuscatorReportGenerator extends ReportDecorator {

    private static final String[] firstNames = { "Oliver", "Beatriz", "James", "Aureliano", "Soraya", "Natasha" };
    private static final String[] lastNames = { "Atom", "Pinzón", "Bond", "Buendia", "Montenegro", "Romanoff" };
    private static Random random = new Random();

    public ObfuscatorReportGenerator(SalesReportGenerator generator) {
        super(generator);
    }

    @Override
    public String generate(List<ReportItem> items) {

        /*Vamos a ofuscar los nombres de los clientes antes de generar el reporte.
        * Lo haremos en una nueva lista para no afectar la original.
        *
        * Las siguientes líneas muestran el código donde hacemos la decoración.
        * */

        List<ReportItem> newList = new ArrayList<>();
        for(ReportItem item: items) {
            ReportItem newItem = new ReportItem(item.getSaleId(),
                                                obfuscateName(),
                                                item.getDate(),
                                                item.getAmount());
            newList.add(newItem);
        }


        //En algún punto, se debe llamar al objeto decorado, ya que es el que tiene la lógica base.
        SalesReportGenerator generator = getTargetGenerator();
        return generator.generate(newList);
    }

    private String obfuscateName() {
        String firstName = firstNames[random.nextInt(6)];
        String lastName = lastNames[random.nextInt(6)];
        return firstName + " " + lastName;
    }
}
