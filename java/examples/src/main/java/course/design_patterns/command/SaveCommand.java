package course.design_patterns.command;

/**
 * Comando concreto (concrete command).
 *
 * Es un intermediario entre el invocador y el receptor de la acción. En casos sencillos, el comando concreto ejecuta
 * directamente la acción y no tiene receptor.
 *
 * @author Manuel Zapata
 */

class SaveCommand implements Command {

    //Este es el receptor de la acción.
    private ProductService service;

    /**
     * El receptor se pasa como parámetro en el constructor (inyección de dependencias), para que sea posible
     * cambiarlo en tiempo de ejecución, sin tener que modificar el código.
     * */
    public SaveCommand(ProductService service) {
        this.service = service;
    }

    @Override
    public void execute(Product product) {
        //En algún punto de la ejecución del comando, se llamará el receptor para que termine la acción solicitada
        // por el invocador.
        service.save(product);
    }
}
