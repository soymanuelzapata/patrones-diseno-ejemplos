package course.design_patterns.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Objeto compuesto (composite).
 *
 * Clase que representa un componente complejo, el cual puede tener objetos relacionados gracias a la composición.
 * Esos objetos pueden ser tanto hojas como otros objetos complejos. A esos objetos relacionados también se les llama
 * hijos.
 *
 * Normalmente estas clases delegan el trabajo a los objetos relacionados, y luego se encargan de consolidar los
 * resultados.
 *
 * @author Manuel Zapata
 */
class Manager extends Employee {

    //En esta colección se guardarán los componentes relacionados.
    private List<Employee> teamMembers;

    public Manager(String name, String positionName) {
        super(name, positionName);
        teamMembers = new ArrayList<>();
    }

    /**
     * Implementa el comportamiento establecido en el componente padre, delegando el trabajo a los componentes
     * relacionados.
     * */
    @Override
    void print() {

        String message = getName() + " - " + getPositionName() + " - " + getCurrentProject();
        System.out.println(message);
        for(Employee member: teamMembers) {
            System.out.print("|-");
            //En la linea siguiente es donde se delega el trabajo.
            member.print();
        }

    }

    /**
     * Implementa el comportamiento establecido en el componente padre, delegando el trabajo a los componentes
     * relacionados.
     * */
    @Override
    void assignProject(String projectName) {

        for (Employee member: teamMembers) {
            member.assignProject(projectName);
        }
        this.setCurrentProject(projectName);
    }

    /**
     * Se implementa el mecanismo para agregar componentes a la jerarquía.
     * */
    @Override
    void add(Employee employee) {
        teamMembers.add(employee);
    }

    /**
     * Se implementa el mecanismo para remover componentes de la jerarquía.
     * */
    @Override
    void remove(Employee employee) {
        teamMembers.remove(employee);
    }

    /**
     * Obtiene un componente de la lista de componentes relacionados.
     * */
    @Override
    Employee getEmployee(int index) {
        Employee result = teamMembers.get(index);
        return result;
    }
}
