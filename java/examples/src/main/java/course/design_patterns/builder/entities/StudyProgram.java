package course.design_patterns.builder.entities;

/**
 * Esta clase no hace parte de la estructura del patrón, pero nos ayuda a darle forma al producto que deben crear
 * los constructores.
 *
 * @author Manuel Zapata
 */
public class StudyProgram {

    private int programId;
    private int universityId;

    public StudyProgram(int programId, int universityId) {
        this.programId = programId;
        this.universityId = universityId;

    }

}
