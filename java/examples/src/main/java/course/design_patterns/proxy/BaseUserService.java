package course.design_patterns.proxy;

import java.util.ArrayList;

/**
 *
 * Sujeto real (real subject).
 *
 * Esta clase representa el sujeto real, es decir, es la clase que queremos proteger a través del proxy.
 *
 * Debe implementar la interfaz que representa el sujeto. En este caso, UserService.
 *
 * @author Manuel Zapata
 *
 * */
class BaseUserService implements UserService {

    private ArrayList<String> users;

    public BaseUserService() {
        users = new ArrayList<>();
    }


    /**
     * Aquí ya se implementa la funcionalidad a controlar.
     * **/
    @Override
    public boolean save(String username) {

        boolean result = false;
        if(!users.contains(username)) {
            users.add(username);
            result = true;
        }
        return result;

    }

}
