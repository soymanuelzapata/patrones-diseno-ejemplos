package course.design_patterns.state;

import java.time.Duration;

/**
 * Estado concreto (concrete state).
 *
 * Representa uno de los estados concretos del contexto (clase MicrowaveOven en este caso). Implementa la interfaz
 * State.
 *
 * En este ejemplo en particular, la clase representa el estado en el cual un microonandas está calentando.
 *
 * @author Manuel Zapata
 * */
class HeatingState implements State {

    /**
     * Referencia al contexto para el cual representa el estado. Se utiliza para pedir información al contexto, y
     * también indicarle cuál es el siguiente estado.
     *
     * Es posible implementar el patrón sin tener este atributo. En ese caso, el contexto sería el responsable de los
     * cambios de estado.
     *
     * */
    private MicrowaveOven context;

    /**
     * El contexto al que pertenece el estado debe ser inyectado como dependencia. En este caso, pasado como
     * parámetro en el constructor.
     * */
    public HeatingState(MicrowaveOven context) {
        this.context = context;
    }


    /**
     * Este método y los siguientes se implementan de acuerdo a lo que esté definido en la interfaz State, y a la
     * lógica de negocio.
     * */
    @Override
    public boolean start(Duration duration) {
        return false;
    }

    @Override
    public boolean stop() {
        //Aquí habría algún tipo de lógica.

        /*
         * Se le indica al contexto cuál debe ser su nuevo estado.
         *
         * En otras implementados es normal que cada estado concreto sea un Singleton.
         * */
        context.changeState(new NotHeatingState(context));
        return true;
    }

    @Override
    public boolean openDoor() {
        return false;
    }
}
